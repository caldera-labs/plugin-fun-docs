---
title: plugin.fun
---
# plugin.fun - by [Caldera Labs](http://CalderLabs.org)

Makes WordPress Plugins

## Who Is This For And Not For

This is functional, but needs some work, so mainly it's for Josh and Nico. Right now.g

Soon, this will be for anyone creating a plugin to extend [Caldera Forms](https://calderaforms.com).

This tool uses Webpack, via the Vue cli. It does require that you be familiar with and have installed [some build tools](/install/dependencies) like npm and Composer. Once the plugin is generated, there is minimal boilerplate needed. For Caldera Forms add-on plugins, this means just providing fields and the logic to do something with your processor or field, which is explained later.

This is not for non-developers, or beginner developers.

## Getting Started

### Install and Quick Start
TL;DR Make a new plugin called "roy" do this and follow prompts
```
git clone git@gitlab.com:caldera-labs/fun.git
vue init ./fun roy
```
<p class="tip">
    For more information see <a href="https://gitlab.com/caldera-labs/fun/blob/master/README.md">Plugin Readme</a>
</p>

Or -- working enough for now CLI -

```
git clone git@gitlab.com:caldera-labs/fun.git
cd fun/bin
bash fun.sh roy
``` 

## Key Concepts

<ul>
     <li>
        <a router-link="/caldera-forms/key-concepts">
            Key Concepts And Best Practices Of Caldera Forming With plugin.fun
        </a>
    </li>
    <li>
        <a router-link="/key-concepts">
            Key Concepts And Best Practices Of Plugin Development With plugin.fun
        </a>
    </li>
</ul>


## Creating Caldera Forms Add-on Plugins
This generator can be used to add one or more of the following types of new capabilities to Caldera Forms:

* Add a custom field type to Caldera Forms.
* Add a payment processor to Caldera Forms.
* Add a CRM processor to Caldera Forms.
* Add a email marketing/ newsletter  processor to Caldera Forms.
* Add a custom validation processor to Caldera Forms.
* Add a generic processor to Caldera Forms.


<p class="tip">
    For more information, see <a router-link="/caldera-forms/index">the section of the docs about making Caldera Forms add-ons</a>.
</p>



## Using With Other Plugins
Right now, I think this does not make sense to be used unless you're making something to extend Caldera Forms. Making this more general, or options for other plugins' add-ons will happen, but not yet.


