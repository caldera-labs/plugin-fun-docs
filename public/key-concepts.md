---
title: Key Concepts
---
# Key Concepts of Plugin Development With plugin.fun
<a router-link="/caldera-forms/key-concepts">
    Key Concepts And Best Practices Of Caldera Forming With plugin.fun
</a>
## The Container
The container creates "globalish state". plugin.fun acknowledges that the global state employed in traditionally-patterned WordPress code is both useful, easy and a major architectural problem from which technical debt flows bountifully. The `Container` class is a reusable and replacable container, based on Pimple. `Container` does not manage its own state like a singleton does. It does have a main instance that is globally-available via the static accessor function dynamically created by plugin.fun generation.

<p class="warning">
   Currently the Container that is generated mainly handles the plugin.fun system logic, that MUST get moved to its own container, so that the generated plugin's container is only for it's functionality.
</p>

 
## Avoid Array Representations of Entities

## Entities Should Have Published Schemas

## Models and Entities and Collections Should Be Different classes

* Entities should create a object representation of an object only.

* A model should collect an entity and its logic -- DB interaction, linked factories, etc. each implemented in their own single responsibility classes).

* A collection should collect entities or models and link to the relevant factory.

* A factory should create objects only.

## Externalize Everything With Composer & NPM

Everything that is currently in plugin.fun, that doesn't actually need to change for concrete-implimentation should be moved into composer or npm packages so that:

1) We can reuse them outside of plugin.fun generated plugins.
2) We can update code of plugin.fun generatated plugins wihout employing

## Always Use Events System For Hooks