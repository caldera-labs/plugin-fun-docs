---
title: Global Settings Pages For Caldera Forms Add-on Plugins
nav: caldera
---
# Creating A Caldera Forms Submenu Page For An Add-ons Global Settings
By default a sub-menu page of the Caldera Forms menu page will be added for your add-on.

On this page, the user interface is generated with VueJS. Read/write of data is handled via the WordPress REST API. A GET and POST endpoint -- requiring `mangage_options` capability is provided, auto-wired to Options API storage. You MUST whitelist all keys. You SHOULD sanitize the data while you are in there.

## REST API Endpoints

## Settings Storage

## Adding Fields

## Composing Components