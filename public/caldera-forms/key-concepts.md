---
title: Key Concepts
nav: caldera
---
# Key Concepts of Caldera Forming With plugin.fun
 <a router-link="/key-concepts">
     Key Concepts And Best Practices Of Plugin Development With plugin.fun
 </a>
 
## Avoid array representations of entities
Caldera Forms represents a lot of data as arrays. For example, form configuration is an array. This makes for a very flexible system, but also means we have to do a ton of repetitive validation and make a lot of assumptions. Many key goals of plugin.dot fun can not be accomplished without object-representations of common entities -- forms, fields, processor settings, etc.

The process of creating an schema, entity, model and collection, will take time, and take place in the `caldera-interop` library. For now, the minimal requirement is to use the `ArrayLike\ArrayLike` class that implements `\ArrayInterface`. For example, `ArrayLike\Form` extends `ArrayLike\ArrayLike` and uses the `ArrayLike\FromArray` trait. This trait gives us the factory method `maybeFromArray`, allowing us to take an `array` or `ArrayLike\ArrayLike` and get an `ArrayLike\ArrayLike` back:

```php
$form = \Caldera_Forms_Forms::get_forms( 'cf12345' );
$form = Form::maybeFromArray( $form );

```

If your array does not have an `ArrayLike\ArrayLike`:
* If your implimentation will be useful to other plugins created with plugin.fun, submit a pull request to plugin.fun to add it.
* If your implimentation will not be useful to other plugins created with plugin.fun, extend ``ArrayLike\ArrayLike` in your plugin.
* Sometimes, even though it smells bad, just use `ArrayLike\ArrayLike`, it's intentionally not abstract.


<p class="tip">
 This is an aspirational standard that we are working towards. It is not implimented in plugin.fun totally yet.
</p> 