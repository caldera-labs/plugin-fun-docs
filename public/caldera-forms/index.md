---
title: Caldera Forms Overview
nav: caldera
---
# Caldera Forms dot plugin dot fun

<a class="docute-button" router-link="/caldera-forms/field-types">
    Add Field Type To Caldera Forms
</a>

<a class="docute-button" router-link="/caldera-forms/field-types">
    Add A Processor To Caldera Forms
</a>

<h3>Built Something Cool?</h3>
<em>Grow with Caldera</em>.
<div>
    <a href="https://calderaforms.com/contact" class="docute-button docute-button-primary">
        Join The Caldera Forms 3rd-Party Developer Program
    </button>

</div>

## Key Goals Of Caldera Forming With plugin.fun

* Add Declarative Structure To Entities
* Standards SHOULD be programmatically enforced, not based on adherence to shared rules.
* ONLY write code unique to a specific feature and even that code should be frictionless-reusable.
* The abstract portion (repetitive) of adding a feature MUST be invisible, auto-magic, and automatically tested in the concrete-context.
* Testing MUST be a feature of the concrete implementation of a feature in a way that MUST be able to run in development and in end-user sites (when running in end-user environment testing MUST annonymize data and MUST be done with explicit consent of user.) 

## Key Concepts of Caldera Forming With plugin.fun

<a class="docute-button" router-link="/caldera-forms/key-concepts">
    Key Concepts
</a>