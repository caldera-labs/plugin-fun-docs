---
title: Creating Caldera Forms Processors
nav: caldera
---

# How To Create Plugins That Add Processors To Caldera Forms

## Prerequisites
You have installed the [required dependencies](http://docs.plugin.fun/#/install/dependencies)

## Quickstart

### Install
```
 git clone git@gitlab.com:caldera-labs/fun.git
 cd fun/bin
 bash fun.sh cf-process-something
```

You can create none or more of the following types of processors when you run the generator:
* A payment processor
    - Answer yes when prompted.
* A newsletter/ email marketing processor
    -Answer yes when prompted.
* A CRM processor
    -Answer yes when prompted.
* A custom validation processor
    - jk, feature not implemented
* A generic processor
    -Answer whatever you want, nothing happens.

Select the appropriate prompt for your needs.

 Note: At this time you will get all of the code for adding each processor and a new field type. That will be fixed later. Just delete what you didn't actually ask for. It's not running, so it's safe to delete it.

 ```
 cd ../../cf-process-something && npm run init

 ```

### Change Things

<p class="tip">
  <strong>Key Concept</strong> Each type of processor you might have created has a sub directory of `src/Processors/` named for it. For example, if you created a payment processor, it can be configured using the files `src/Processors/Payment`.
</p>


#### `src/Processors/{type}/config.php`
This generates the processor's settings UI in the Caldera Forms editor. You SHOULD not change this.

#### `src/Processors/{type}/Controller.php`
You SHOULD change this. This class does the actual processing. In fact, if you want your plugin to actually do anything, this is where you will make it do things.

<p class="tip">
    This is where you implement your feature.
</p>

See the section below "The Data Object" for more information about the object passed to all controller callback functions.

#### `src/Processors/{type}/Processor.php`
This class represents the processor's configuration that is injected to most other classes in the [automagic processor routing system](https://hiroy.club).

If you want to change the meta details -- name, description, author or author URL, you MUST modify the `getMetaDetails` method.

If you want to change the fields that are available in the processor's UI in the form editor, you MUST modify the `getAdminFields` method.

<p class="tip">
    This is where you create the administrate interface for your feature.
</p>

The array you are creating is passed to [Caldera_Forms_Processor_UI::config_fields()](https://github.com/CalderaWP/Caldera-Forms/blob/master/processors/classes/ui.php#L23) and each field is rendered with [Caldera_Forms_Processor_UI::config_field()](https://github.com/CalderaWP/Caldera-Forms/blob/master/processors/classes/ui.php#L71). See inline docs in source.

This will change in the near future, but the configuration SHOULD generally stay the same for v1.

### The Data Object
Each callback function in the controller is passed an object of the calderawp\\{{name}}\Processors\Data class. *The controller is where you put the code that implements the feature of your plugin *. For example, if your plugin processes payments against a 3rd-party API, you would use their PHP SDK to make the necessary API calls in the controller.

Let's say that the controller is where your feature implementation passes into and out of your feature implementation. If you can not do what you need to do with this object, __and you might not, this is all very new__ submit a pull request to make it possible.

`Data` provides the following public methods:
* `getFields`
    ** This method returns the admin field configurations. This is the same info used to generated UI. It __does not__ relate to the current submission.
    ** An `array` or array-like object will be returned.
* `getValues`
    ** This method returns values of each registered fields of the current submission.
    ** An `array` or array-like object will be returned.
* `getForm`
    ** This method returns the form configuration for the form that is processing currently.
    ** An `array` or array-like object will be returned.
* `addError`
    ** This method is used to add an error. Adding this error, at pre-process, or process, will trigger a validation error and stop submission. At the post-process state, this method has no effect and it is too late to add errors.
    ** Pass a `string`.
* `getErrors`
    ** This method returns an array of errors, if any.

The `Data` object MUST be the ONLY way data is passed to yo
## Advanced Topics
@TODO -- Really cool stuff going on here, lot's of possible fun.