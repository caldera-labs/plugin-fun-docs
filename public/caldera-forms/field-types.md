---
title: Adding New Field Types For Caldera Forms
nav: caldera
---

# How To Generate A Plugin That Adds A New Field Type To Caldera Forms

## Prerequisites
You have installed the [required dependencies](../setup/dependencies.md)

## Quickstart

### Create Plugin To Add A New Field Type
```
 git clone git@gitlab.com:caldera-labs/fun.git
 cd fun/bin
 bash fun.sh cf-something-field
 ```
 
 In the prompts, answer "Yes" to the question "Create field?".
 
 Note: At this time you will get all of the code for adding processors. That will be fixed later. Just delete it. It's not running, so it's safe to delete it.
 
 Then install new project:
 
 ```
 cd ../../cf-something-field && npm run init
 
 ```

### Change Things
#### Front-end

If you need to change the way the field shows up in the front-end, go to `/src/Field/front.php`

* If all you need to do is change HTML attributes of an input, then don't change anything here.
* If you don't need just a simple input, make heavy edits here, this file generates the HTML for the field in the front-end.

#### HTML Attributes Of Field
If you need to change HTML attributes and are using the included HTML generator for input fields from Caldera Forms, then you should go into `src/Field/Register.php` and add additional attributes to add in the `filterAttrs` method, which is hooked to the ["caldera_forms_field_attributes-$fieldType"](https://calderaforms.com/doc/caldera_forms_field_attributes/) filter.

#### Editor Fields
If you need to add (or remove the demonstration fields) that show in the Caldera Forms form editor when editing a field of this new type, go to `src\Field\Field.php`.

In the `getAdminFields` method, you can define which fields show up in the editor, under the default field types. This array is passed to [Caldera_Forms_Processor_UI::config_fields()](https://github.com/CalderaWP/Caldera-Forms/blob/master/processors/classes/ui.php#L23) and each field is rendered with [Caldera_Forms_Processor_UI::config_field()](https://github.com/CalderaWP/Caldera-Forms/blob/master/processors/classes/ui.php#L71). See inline docs in source.

This will change in the near future, but the configuration SHOULD generally stay the same for v1.

If you need to change the definition of the field -- it's name, where it's files are (please don't, the goal is to make this an upgradable, generated plugin,) what CSS or JavaScript to load for it, etc, modify the `getConfig` which is heavily documented inline and also see.

#### Also See
[This is the relevant Caldera Forms docs](https://calderaforms.com/doc/create-field-type-caldera-forms/) that you should see.

## Advanced Topics
@TODO