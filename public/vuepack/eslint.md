---
title: Eslint
nav: vuepack
---
_We are not currently using Eslint. We should and we will._
__This part of the documentation, like much of the Webpack stuff, is copied form [VuePack](https://github.com/egoist/vuepack).__

# ESLint

The default eslint config is [eslint-config-vue](https://github.com/vuejs/eslint-config-vue), but you can change it to anything you want in `.eslintrc`.
